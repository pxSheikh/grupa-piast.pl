# Project Name: grupa-piast.pl website

## Introduction

**grupa-piast.pl** marks a significant milestone in my web development journey, dating back to 2016. As my inaugural commercial project, this website serves as a testament to my early endeavors in the digital realm.

## Project Overview

- **Inception Year:** 2016
- **Type:** One-Page Website
- **URL:** [grupa-piast.pl](https://grupa-piast-pl-pxsheikh-9c8eea77a7a19b580d785c6a7cff1f89c4e59a4.gitlab.io/)

## About the Project

_In 2016, I developed_ **grupa-piast.pl** _to provide comprehensive information about the services offered by the company, including event organization, catering, and a beer hall. The website features a clear and user-friendly design, ensuring easy access to service details, menus, and contact information._

## Tech Stack

- HTML
- CSS
- JavaScript